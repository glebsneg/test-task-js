function task_1(array: number[]): number[] {
    let reverseArray: number[] = [];

    for (let i = array.length - 1; i >= 0; i--) {
        reverseArray.push( array[i] );
    }
    return reverseArray;
}

console.log( 'task_1 : ', task_1( [1, 2, 3, 4] ) );

function task_2<T>(array_1: T[], array_2: T[]): boolean {
    let uniqueArray_1: T[] = [];
    let uniqueArray_2: T[] = [];

    for (const item of array_1) {
        if (!uniqueArray_1.includes( item )) {
            uniqueArray_1.push( item );
        }
    }

    for (const item of array_2) {
        if (!uniqueArray_2.includes( item )) {
            uniqueArray_2.push( item );
        }
    }

    return uniqueArray_1.sort().join( '' ) == uniqueArray_2.sort().join( '' );
}

console.log( 'task_2 : ', task_2( [1, 2, 2, 1], [2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2] ) );

interface Person {
    id: number,
    value: any,
}
interface PersonCountNumbersAndStrings {
    id: number,
    number: number,
    string: number,
}
function task_3(array: Person[]): PersonCountNumbersAndStrings[] {
    let result: PersonCountNumbersAndStrings[] = [];

    for (const person of array) {
        const numberValue = +(typeof (person.value) === "number");
        const stringValue = +(typeof (person.value) === "string");
        const indexInResultArray = result.findIndex( resultPerson => resultPerson.id === person.id );

        if (indexInResultArray == -1) {
            result.push( {id: person.id, number: numberValue, string: stringValue} )
        } else {
            result[indexInResultArray].number += numberValue;
            result[indexInResultArray].string += stringValue;
        }
    }
    return result;
}

console.log( 'task_3 : ', task_3( [
    {id: 1, value: 2},
    {id: 1, value: 'asd'},
    {id: 1, value: 'asd'},
] ) );

function task_4() {

}

function task_5(aString: string, character: string): number[] {
    let result: number[] = [];
    let index: number = -1;

    while ((index = aString.indexOf( character, index + 1 )) !== -1) {
        result.push( index );
    }

    return result;
}

console.log( 'task_5 : ', task_5( 'asdasdasdasd', 'a' ) );

function task_6(aString: string, character: string): number {
    let result: number = 0;
    let index: number = -1;
    aString = aString.toLowerCase();
    character = character.toLowerCase();

    while ((index = aString.indexOf( character, index + 1 )) !== -1) {
        result += 1;
    }

    return result;
}

console.log( 'task_6 : ', task_6( 'addddAddaa', 'a' ) );

interface currencyGroup {
    currency: string,
    value: any,
}
function task_7(array: currencyGroup[]): string[] {
    let result: currencyGroup[] = [];

    for (const currencyGroup of array) {
        const indexInResultArray = result.findIndex( resultPerson => resultPerson.currency === currencyGroup.currency );
        currencyGroup.value = +currencyGroup.value;
        if (!isNaN( currencyGroup.value )) {

            if (indexInResultArray == -1) {
                result.push( {currency: currencyGroup.currency, value: currencyGroup.value} )
            } else {
                result[indexInResultArray].value += currencyGroup.value;
            }
        }
    }

    return result.map( result => `${result.currency}:${result.value}` );
}

console.log( 'task_7 : ', task_7( [
    {currency: 'USD', value: 2},
    {currency: 'UAH', value: 3},
    {currency: 'EUR', value: 1.02},
    {currency: 'USD', value: 1.01},
    {currency: 'USD', value: '1.01'},
    {currency: 'USD', value: 'qwe1.01'},
    {currency: 'USD', value: true},
    {currency: 'USD', value: 'q'},
] ) )

function task_8(...args: any): string {
    let result: string = '';

    for (const argument of Array.from( arguments )) {
        if (typeof (argument) === "number" || typeof (argument) === "string") {
            result += argument
        }
    }
    return result;
}

console.log( 'task_8 : ', task_8( 1, 'asd', 'ff', ' ', null, false ) );

interface Bool {
    id: number,
    done: boolean,
}
function task_9(array: Bool[]): boolean {
    for (let i = 0; i < array.length; i++) {
        if (array[i].done === false) {
            return false;
        }
    }
    return true;
}

console.log( 'task_9 : ', task_9( [
    {id: 0, done: true},
    {id: 1, done: true},
    {id: 2, done: true},
    {id: 3, done: false},
] ));

function task_10(aString: string, shift: number): string {
    let result: string = '';
    let aStringArray: string[] = aString.split( '' );

    function getCharEntryToTheAlphabetCharCodes(charCode: number): number {
        const alphabetCharCodes = {
            engLowercaseStart: 97, engLowercaseEnd: 122,
            engUppercaseStart: 65, engUppercaseEnd: 90,
        };

        const arrayOfAlphabetCharCodes: [number, number][] = [
            [alphabetCharCodes.engLowercaseStart, alphabetCharCodes.engLowercaseEnd],
            [alphabetCharCodes.engUppercaseStart, alphabetCharCodes.engUppercaseEnd],
        ];

        for (const AlphabetCharCodes of arrayOfAlphabetCharCodes) {
            if (charCode >= AlphabetCharCodes[0] && charCode <= AlphabetCharCodes[1]) {

                if (charCode - shift < AlphabetCharCodes[0]) {
                    return AlphabetCharCodes[1] - AlphabetCharCodes[0] + charCode - (shift - 1);
                } else {
                    return charCode - shift;
                }
            }
        }
        return NaN;
    }

    for (const char of aStringArray) {
        const newCharCode: number = getCharEntryToTheAlphabetCharCodes( char.charCodeAt( 0 ) );

        if (!isNaN( newCharCode )) {
            result += String.fromCharCode( newCharCode );
        } else {
            result += char;
        }
    }

    return result;
}

console.log( 'task_10 :', task_10( 'DE aA!', 3 ) );

export {task_1, task_2, task_3, task_4, task_5, task_6, task_7, task_8, task_9, task_10};
