"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.task_10 = exports.task_9 = exports.task_8 = exports.task_7 = exports.task_6 = exports.task_5 = exports.task_4 = exports.task_3 = exports.task_2 = exports.task_1 = void 0;
function task_1(array) {
    let reverseArray = [];
    for (let i = array.length - 1; i >= 0; i--) {
        reverseArray.push(array[i]);
    }
    return reverseArray;
}
exports.task_1 = task_1;
console.log('task_1 : ', task_1([1, 2, 3, 4]));
function task_2(array_1, array_2) {
    let uniqueArray_1 = [];
    let uniqueArray_2 = [];
    for (const item of array_1) {
        if (!uniqueArray_1.includes(item)) {
            uniqueArray_1.push(item);
        }
    }
    for (const item of array_2) {
        if (!uniqueArray_2.includes(item)) {
            uniqueArray_2.push(item);
        }
    }
    return uniqueArray_1.sort().join('') == uniqueArray_2.sort().join('');
}
exports.task_2 = task_2;
console.log('task_2 : ', task_2([1, 2, 2, 1], [2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2]));
function task_3(array) {
    let result = [];
    for (const person of array) {
        const numberValue = +(typeof (person.value) === "number");
        const stringValue = +(typeof (person.value) === "string");
        const indexInResultArray = result.findIndex(resultPerson => resultPerson.id === person.id);
        if (indexInResultArray == -1) {
            result.push({ id: person.id, number: numberValue, string: stringValue });
        }
        else {
            result[indexInResultArray].number += numberValue;
            result[indexInResultArray].string += stringValue;
        }
    }
    return result;
}
exports.task_3 = task_3;
console.log('task_3 : ', task_3([
    { id: 1, value: 2 },
    { id: 1, value: 'asd' },
    { id: 1, value: 'asd' },
]));
function task_4() {
}
exports.task_4 = task_4;
function task_5(aString, character) {
    let result = [];
    let index = -1;
    while ((index = aString.indexOf(character, index + 1)) !== -1) {
        result.push(index);
    }
    return result;
}
exports.task_5 = task_5;
console.log('task_5 : ', task_5('asdasdasdasd', 'a'));
function task_6(aString, character) {
    let result = 0;
    let index = -1;
    aString = aString.toLowerCase();
    character = character.toLowerCase();
    while ((index = aString.indexOf(character, index + 1)) !== -1) {
        result += 1;
    }
    return result;
}
exports.task_6 = task_6;
console.log('task_6 : ', task_6('addddAddaa', 'a'));
function task_7(array) {
    let result = [];
    for (const currencyGroup of array) {
        const indexInResultArray = result.findIndex(resultPerson => resultPerson.currency === currencyGroup.currency);
        currencyGroup.value = +currencyGroup.value;
        if (!isNaN(currencyGroup.value)) {
            if (indexInResultArray == -1) {
                result.push({ currency: currencyGroup.currency, value: currencyGroup.value });
            }
            else {
                result[indexInResultArray].value += currencyGroup.value;
            }
        }
    }
    return result.map(result => `${result.currency}:${result.value}`);
}
exports.task_7 = task_7;
console.log('task_7 : ', task_7([
    { currency: 'USD', value: 2 },
    { currency: 'UAH', value: 3 },
    { currency: 'EUR', value: 1.02 },
    { currency: 'USD', value: 1.01 },
    { currency: 'USD', value: '1.01' },
    { currency: 'USD', value: 'qwe1.01' },
    { currency: 'USD', value: true },
    { currency: 'USD', value: 'q' },
]));
function task_8(...args) {
    let result = '';
    for (const argument of Array.from(arguments)) {
        if (typeof (argument) === "number" || typeof (argument) === "string") {
            result += argument;
        }
    }
    return result;
}
exports.task_8 = task_8;
console.log('task_8 : ', task_8(1, 'asd', 'ff', ' ', null, false));
function task_9(array) {
    for (let i = 0; i < array.length; i++) {
        if (array[i].done === false) {
            return false;
        }
    }
    return true;
}
exports.task_9 = task_9;
console.log('task_9 : ', task_9([
    { id: 0, done: true },
    { id: 1, done: true },
    { id: 2, done: true },
    { id: 3, done: false },
]));
function task_10(aString, shift) {
    let result = '';
    let aStringArray = aString.split('');
    function getCharEntryToTheAlphabetCharCodes(charCode) {
        const alphabetCharCodes = {
            engLowercaseStart: 97, engLowercaseEnd: 122,
            engUppercaseStart: 65, engUppercaseEnd: 90,
        };
        const arrayOfAlphabetCharCodes = [
            [alphabetCharCodes.engLowercaseStart, alphabetCharCodes.engLowercaseEnd],
            [alphabetCharCodes.engUppercaseStart, alphabetCharCodes.engUppercaseEnd],
        ];
        for (const AlphabetCharCodes of arrayOfAlphabetCharCodes) {
            if (charCode >= AlphabetCharCodes[0] && charCode <= AlphabetCharCodes[1]) {
                if (charCode - shift < AlphabetCharCodes[0]) {
                    return AlphabetCharCodes[1] - AlphabetCharCodes[0] + charCode - (shift - 1);
                }
                else {
                    return charCode - shift;
                }
            }
        }
        return NaN;
    }
    for (const char of aStringArray) {
        const newCharCode = getCharEntryToTheAlphabetCharCodes(char.charCodeAt(0));
        if (!isNaN(newCharCode)) {
            result += String.fromCharCode(newCharCode);
        }
        else {
            result += char;
        }
    }
    return result;
}
exports.task_10 = task_10;
console.log('task_10 :', task_10('DE aA!', 3));
